package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class Stacks implements StackOperations {
    private ArrayList<String> stack;

    public Stacks(ArrayList<String> al) {
        this.stack = al;
    }

    @Override
    public List<String> get() {
        if(stack.size()>0){
            ArrayList<String> reversed = stack;
            Collections.reverse(reversed);
            return reversed;
        }else{
            return null;
        }
    }

    @Override
    public Optional<String> pop() {
        if(stack.size()>0){
            String remove = stack.get(stack.size() - 1);
            stack.remove(stack.get(stack.size() - 1));
            return Optional.ofNullable(remove);
        }else{
            return null;
        }
    }

    @Override
    public void push(String item) {
        stack.add(item);
    }
}
