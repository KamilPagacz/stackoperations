package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Stacks stack = new Stacks(new ArrayList<>());
        System.out.println(stack.get());
        System.out.println(stack.pop());
        stack.push("jeden");
        stack.push("dwa");
        stack.push("trzy");
        System.out.println(stack.get());
        System.out.println(stack.pop());
        System.out.println(stack.get());
    }

}
